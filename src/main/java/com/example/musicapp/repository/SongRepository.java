package com.example.musicapp.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.musicapp.entity.Album;
import com.example.musicapp.entity.Song;

@Repository
public interface SongRepository extends JpaRepository<Song, Long> {
	public List<Song> findBySongTitleContaining(String songTitle);
	public Set<Song> findByAlbum(Optional<Album> album);
}
