package com.example.musicapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.example.musicapp.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	
}
