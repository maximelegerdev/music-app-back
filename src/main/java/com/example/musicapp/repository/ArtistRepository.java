package com.example.musicapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.example.musicapp.entity.Artist;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long>{
	public List<Artist> findByNameContaining(String name);
}
