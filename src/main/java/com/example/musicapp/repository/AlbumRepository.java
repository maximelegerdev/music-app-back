package com.example.musicapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.musicapp.entity.Album;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

	public List<Album> findByAlbumTitleContaining(String albumTitle);

}
