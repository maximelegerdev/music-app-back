package com.example.musicapp.controllers;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicapp.entity.User;
import com.example.musicapp.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	private UserService userService;
	
	public UserController() {
		
	}

	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@GetMapping("{userId}")
	public Optional<User> getUser(@PathVariable("userId") final Long userId) {
		return userService.getUser(userId);
	}
	
	@GetMapping
	public Iterable<User> getUsers(){
		return userService.getUsers();
	}
	
	@PostMapping
	public void updateUser(@RequestBody User user) {
		userService.updateUser(user);
	}
	
	@PutMapping
	public void saveUser(@RequestBody User user) {
		userService.saveUser(user);
	}
	
	@DeleteMapping("{userId}")
	public void deleteArtist(@PathVariable("userId") Long userId) {
		userService.deleteUser(userId);
	}
}

