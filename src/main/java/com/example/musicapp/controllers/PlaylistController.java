package com.example.musicapp.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicapp.entity.Playlist;
import com.example.musicapp.services.PlaylistService;

@RestController
@RequestMapping("/playlist")
public class PlaylistController {
	
	private PlaylistService playlistService;
	
	public PlaylistController() {
	}
	
	@Autowired
	public PlaylistController(PlaylistService playlistService) {
		super();
		this.playlistService = playlistService;
	}
	
	@GetMapping("{playlistId}")
	public Optional<Playlist> getPlaylist(@PathVariable("playlistId") Long playlistId){
		return playlistService.getPlaylist(playlistId);
	}
	
	@GetMapping()
	public Iterable<Playlist> getPlaylists() {
		return playlistService.getPlaylists();
	}
	
	@PostMapping
	public void updatePlaylist(@RequestBody Playlist playlist) {
		playlistService.updatePlaylist(playlist);
	}
	
	@PutMapping
	public void savePlaylist(@RequestBody Playlist playlist) {
		playlistService.savePlaylist(playlist);
	}
	
	@DeleteMapping("{playlistId}")
	public void deletePlaylist(@PathVariable("playlistId") final Long playlistId) {
		playlistService.deletePlaylist(playlistId);
	}
	
	@GetMapping("/search/{playlistName}")
	public List<Playlist> searchPlaylist(@PathVariable("playlistName") String playlistName){
		return playlistService.searchPlaylist(playlistName);
	}
}
