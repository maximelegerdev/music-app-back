package com.example.musicapp.controllers;

import com.example.musicapp.entity.Artist;
import com.example.musicapp.services.AlbumService;
import com.example.musicapp.services.ArtistService;
import com.example.musicapp.services.SongService;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/artist")
public class ArtistController {
	
	private ArtistService artistService;
	private SongService songService;
	private AlbumService albumService;
	
	public ArtistController() {
	}
	
	@Autowired
	public ArtistController(ArtistService artistService, SongService songService, AlbumService albumService) {
		this.setSongService(songService);
		this.artistService = artistService;
		this.setAlbumService(albumService);
	}

	@GetMapping("{artistId}")
	public Optional<Artist> getArtist(@PathVariable("artistId") final Long artistId){
		return artistService.getArtist(artistId);
	}
	
	@GetMapping()
	public Iterable<Artist> getArtists() {
		return artistService.getArtists();
	}
	
	@PostMapping
	public void updateArtist(@RequestBody Artist artist) {
		artistService.updateArtist(artist);
	}
	
	@PutMapping
	public void saveArtist(@RequestBody Artist artist) {
		artistService.saveArtist(artist);
	}
	
	@DeleteMapping("{artistId}")
	public void deleteArtist(@PathVariable("artistId") Long artistId) {
		artistService.deleteArtist(artistId);
	}
	
	//Méthodes supplémentaires hors méthodes CRUD
	
	@GetMapping("/search/{name}")
	public List<Artist> searchArtist(@PathVariable("name") String name){
		return artistService.searchArtist(name);
	}

	public AlbumService getAlbumService() {
		return albumService;
	}

	public void setAlbumService(AlbumService albumService) {
		this.albumService = albumService;
	}

	public SongService getSongService() {
		return songService;
	}

	public void setSongService(SongService songService) {
		this.songService = songService;
	}
	
	
}
