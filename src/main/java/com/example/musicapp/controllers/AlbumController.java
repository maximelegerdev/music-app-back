package com.example.musicapp.controllers;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicapp.entity.Album;
import com.example.musicapp.entity.Artist;
import com.example.musicapp.entity.Song;
import com.example.musicapp.services.AlbumService;

@RestController
@RequestMapping("/album")
public class AlbumController {
	
	private AlbumService albumService;
	
	public AlbumController() {
	}
	
	@Autowired
	public AlbumController(AlbumService albumService) {
		this.albumService = albumService;
	}
	
	@GetMapping("{albumId}")
	public Optional<Album> getAlbum(@PathVariable("albumId") final Long albumId){
		return albumService.getAlbum(albumId);
	}
	
	@GetMapping
	public Iterable<Album> getAlbums(){
		return albumService.getAlbums();
	}
	
	@PostMapping
	public void updateAlbum(@RequestBody Album album) {
		albumService.updateAlbum(album);
	}
	
	@PutMapping
	public void saveAlbum(@RequestBody Album album) {
		albumService.updateAlbum(album);
	}
	
	@DeleteMapping("{albumId}")
	public void deleteAlbum(@PathVariable("albumId") Long albumId) {
		albumService.deleteAlbum(albumId);
	}
	
	//Méthodes supplémentaires hors méthodes CRUD
	
	@GetMapping("/search/{albumTitle}")
	public List<Album> searchArtist(@PathVariable("albumTitle") String albumTitle){
		return albumService.searchAlbum(albumTitle);
	}

	@GetMapping("/albumSongs/{albumId}")
	public Set<Song> getSongsFromAlbum(@PathVariable("albumId") Long albumId){
		return albumService.getSongsFromAlbum(albumId);
	}
}
