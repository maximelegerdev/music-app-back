package com.example.musicapp.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicapp.entity.Song;
import com.example.musicapp.services.ArtistService;
import com.example.musicapp.services.SongService;

@RestController
@CrossOrigin(origins = "http://localhost:3000/")
@RequestMapping(value="/song", produces = "application/json")
public class SongController {
    
    private SongService songService;
    private ArtistService artistService;

    public SongController() {
    }
    
    @Autowired
    public SongController(SongService songService, ArtistService artistService) {
		this.songService = songService;
		this.setArtistService(artistService);
	}

	@GetMapping("{songId}")
    public Optional<Song> getSong(@PathVariable("songId") final Long songId){
        return songService.getSong(songId);
    }
    
    @GetMapping()
    public Iterable<Song> getSongs(){
		return songService.getSongs();
    }
    
    @PostMapping
    public void updateSong(@RequestBody Song song) {
    	songService.updateSong(song);
    }
    
    @PutMapping
    public void saveSong(@RequestBody Song song) {
    	songService.saveSong(song);
    }
    
    @DeleteMapping("{songId}")
    public void deleteSong(@PathVariable("songId") Long songId) {
    	songService.deleteSong(songId);
    }
    
    //Méthodes supplémentaires hors CRUD
    
    @GetMapping("/search/{songTitle}")
    public List<Song> searchSong(@PathVariable("songTitle") String songTitle) {
    	return songService.searchSong(songTitle);
    }

	public ArtistService getArtistService() {
		return artistService;
	}

	public void setArtistService(ArtistService artistService) {
		this.artistService = artistService;
	}
    
    
}