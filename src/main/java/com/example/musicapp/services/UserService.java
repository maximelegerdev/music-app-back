package com.example.musicapp.services;

import java.util.Optional;

import com.example.musicapp.entity.User;

public interface UserService {
	public Optional<User> getUser(final Long userId);
	public Iterable<User> getUsers();
	public void updateUser(User user);
	public void deleteUser(final Long userId);
	public void saveUser(User user);
}
