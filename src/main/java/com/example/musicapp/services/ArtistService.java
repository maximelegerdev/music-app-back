package com.example.musicapp.services;

import java.util.List;
import java.util.Optional;

import com.example.musicapp.entity.Artist;


public interface ArtistService {
	public Optional<Artist> getArtist(final Long artistId);
    public Iterable<Artist> getArtists();
    public void updateArtist(Artist artist);
    public void deleteArtist (final Long artistId);
    public void saveArtist(Artist artist);
    public List<Artist> searchArtist(String name);
}
