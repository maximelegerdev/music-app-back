package com.example.musicapp.services;

import java.util.List;
import java.util.Optional;

import com.example.musicapp.entity.Song;



public interface SongService {

    public Optional<Song> getSong(final Long songId);

    public Iterable<Song> getSongs();
    
    public void updateSong(Song song);

    public void deleteSong (final Long songId);

    public void saveSong(Song song);
    
    public List<Song> searchSong(String songTitle);
}