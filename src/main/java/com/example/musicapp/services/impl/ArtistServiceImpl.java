package com.example.musicapp.services.impl;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.musicapp.entity.Artist;
import com.example.musicapp.repository.ArtistRepository;
import com.example.musicapp.services.ArtistService;

@Service
public class ArtistServiceImpl implements ArtistService{
	
	private ArtistRepository artistRepository;
	
	public ArtistServiceImpl() {
	}
	
	@Autowired
	public ArtistServiceImpl(ArtistRepository artistRepository) {
		super();
		this.artistRepository = artistRepository;
	}

	@Override
	public Optional<Artist> getArtist(final Long artistId){
        return artistRepository.findById(artistId);
    }

	@Override
    public Iterable<Artist> getArtists() {
        return artistRepository.findAll();
    }
	
	@Override
	public void updateArtist (Artist artist) {
		artistRepository.save(artist);
	}	

	@Override
    public void deleteArtist (final Long artistId) {
    	artistRepository.deleteById(artistId);
    }

	@Override
    public void saveArtist(Artist artist){
        artistRepository.save(artist);
    }
	
	@Override
	public List<Artist> searchArtist(String name) {
		return artistRepository.findByNameContaining(name);
	}

}
