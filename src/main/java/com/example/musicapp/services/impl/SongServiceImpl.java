package com.example.musicapp.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.musicapp.entity.Song;
import com.example.musicapp.repository.SongRepository;
import com.example.musicapp.services.SongService;

@Service
public class SongServiceImpl implements SongService{
	
	private SongRepository songRepository;

	public SongServiceImpl() {
	}
	
	@Autowired
	public SongServiceImpl(SongRepository songRepository) {
		super();
		this.songRepository = songRepository;
	}

	@Override
    public Optional<Song> getSong(final Long songId){
        return songRepository.findById(songId);
    }

	@Override
    public Iterable<Song> getSongs() {
        return songRepository.findAll();
    }
	
	@Override
	public void updateSong(Song song) {
		songRepository.save(song);
	}

	@Override
	public void deleteSong (final Long songId) {
        songRepository.deleteById(songId);
    }

	@Override
    public void saveSong(Song song){
    	songRepository.save(song);
    }
	
	@Override
	public List<Song> searchSong(String songTitle) {
		return songRepository.findBySongTitleContaining(songTitle);
	}

}
