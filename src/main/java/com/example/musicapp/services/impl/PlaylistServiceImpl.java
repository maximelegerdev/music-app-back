package com.example.musicapp.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.musicapp.entity.Playlist;
import com.example.musicapp.repository.PlaylistRepository;
import com.example.musicapp.services.PlaylistService;

@Service
public class PlaylistServiceImpl implements PlaylistService{

	private PlaylistRepository playlistRepository;
	
	public PlaylistServiceImpl() {
	}
	
	@Autowired
	public PlaylistServiceImpl(PlaylistRepository playlistRepository) {
		super();
		this.playlistRepository = playlistRepository;
	}
	
	@Override
	public Optional<Playlist> getPlaylist(Long playlistId) {
		return playlistRepository.findById(playlistId);
	}

	@Override
	public Iterable<Playlist> getPlaylists() {
		return playlistRepository.findAll();
	}

	@Override
	public void updatePlaylist(Playlist playlist) {
		playlistRepository.save(playlist);
	}

	@Override
	public void deletePlaylist(Long playlistId) {
		playlistRepository.deleteById(playlistId);
	}

	@Override
	public void savePlaylist(Playlist playlist) {
		playlistRepository.save(playlist);
	}
	
	@Override 
	public List<Playlist> searchPlaylist(String playlistName) {
		return playlistRepository.findByPlaylistNameContaining(playlistName);
	}

}
