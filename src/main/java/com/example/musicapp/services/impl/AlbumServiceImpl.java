package com.example.musicapp.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.musicapp.entity.Album;
import com.example.musicapp.entity.Song;
import com.example.musicapp.repository.AlbumRepository;
import com.example.musicapp.repository.SongRepository;
import com.example.musicapp.services.AlbumService;

@Service
public class AlbumServiceImpl implements AlbumService {

	private AlbumRepository albumRepository;
	private SongRepository songRepository;
	
	public AlbumServiceImpl() {
		
	}
	
	@Autowired
	public AlbumServiceImpl(AlbumRepository albumRepository, SongRepository songRepository) {
		super();
		this.albumRepository = albumRepository;
		this.songRepository = songRepository;
	}
	
	@Override
	public Optional<Album> getAlbum(Long albumId) {
		return albumRepository.findById(albumId);
	}

	@Override
	public Iterable<Album> getAlbums() {
		return albumRepository.findAll();
	}

	@Override
	public void updateAlbum(Album album) {
		albumRepository.save(album);
	}

	@Override
	public void deleteAlbum(final Long albumId) {
		albumRepository.deleteById(albumId);
	}

	@Override
	public void saveAlbum(Album album) {
		albumRepository.save(album);
	}
	
	@Override 
	public List<Album> searchAlbum(String albumTitle) {
		return albumRepository.findByAlbumTitleContaining(albumTitle);
	}

	@Override 
	public Set<Song> getSongsFromAlbum(final Long albumId){
		Optional<Album> album = albumRepository.findById(albumId);
		return songRepository.findByAlbum(album);
	}
}
