package com.example.musicapp.services.impl;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.musicapp.entity.User;
import com.example.musicapp.repository.UserRepository;
import com.example.musicapp.services.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private UserRepository userRepository;
	
	public UserServiceImpl() {
	}

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}
	
	@Override
	public Optional<User> getUser(final Long userId){
        return userRepository.findById(userId);
    }

	@Override
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }
	
	@Override
	public void updateUser (User user) {
		userRepository.save(user);
	}	

	@Override
    public void deleteUser (final Long userId) {
		userRepository.deleteById(userId);
    }

	@Override
    public void saveUser(User user){
		userRepository.save(user);
    }
	
}
