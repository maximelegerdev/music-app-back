package com.example.musicapp.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.musicapp.entity.Album;
import com.example.musicapp.entity.Song;

public interface AlbumService {
	public Optional<Album> getAlbum(final Long albumId);
	public Iterable<Album> getAlbums();
	public void updateAlbum(Album album);
	public void deleteAlbum(final Long albumId);
	public void saveAlbum(Album album);
	public List<Album> searchAlbum(String albumTitle);
	public Set<Song> getSongsFromAlbum(final Long albumId);
}
