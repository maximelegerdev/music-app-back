package com.example.musicapp.services;

import java.util.List;
import java.util.Optional;

import com.example.musicapp.entity.Playlist;

public interface PlaylistService {

	public Optional<Playlist> getPlaylist(final Long playlistId);
	
	public Iterable<Playlist> getPlaylists();
	
	public void updatePlaylist(Playlist playlist);
	
	public void deletePlaylist(final Long playlistId);
	
	public void savePlaylist(Playlist playlist);
	
	public List<Playlist> searchPlaylist(String playlistName);
	
}
