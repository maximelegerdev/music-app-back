package com.example.musicapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	public User() {
	}

	public User(Long userId, String username, String password, String picture, String likedPlaylists, String likedAlbums,
			String likedArtists, String likedSongs, String recentlyPlayed, int userStatus, String pictureAlt) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.picture = picture;
		this.likedPlaylists = likedPlaylists;
		this.likedAlbums = likedAlbums;
		this.likedArtists = likedArtists;
		this.likedSongs = likedSongs;
		this.recentlyPlayed = recentlyPlayed;
		this.userStatus = userStatus;
		this.pictureAlt = pictureAlt;
	}

	@Id
	@Column(name="user_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	
	@Column(name="username", nullable = false)
	private String username;
	
	@Column(name="password", nullable = false)
	private String password;
	
	@Column(name="picture", nullable = false)
	private String picture;
	
	@Column(name="liked_playlists")
	private String likedPlaylists;
	
	@Column(name="liked_albums")
	private String likedAlbums;

	@Column(name="liked_artists")
	private String likedArtists;
	
	@Column(name="liked_songs")
	private String likedSongs;
	
	@Column(name="recently_played")
	private String recentlyPlayed;
	
	@Column(name="user_status", nullable = false)
	private int userStatus;
	
	@Column(name="picture_alt", nullable = false)
	private String pictureAlt;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public int getUserStatus() {
		return userStatus;
	}
	
	public String getLikedPlaylists() {
		return likedPlaylists;
	}

	public void setLikedPlaylists(String likedPlaylists) {
		this.likedPlaylists = likedPlaylists;
	}

	public String getLikedAlbums() {
		return likedAlbums;
	}

	public void setLikedAlbums(String likedAlbums) {
		this.likedAlbums = likedAlbums;
	}

	public String getLikedArtists() {
		return likedArtists;
	}

	public void setLikedArtists(String likedArtists) {
		this.likedArtists = likedArtists;
	}

	public String getLikedSongs() {
		return likedSongs;
	}

	public void setLikedSongs(String likedSongs) {
		this.likedSongs = likedSongs;
	}

	public String getRecentlyPlayed() {
		return recentlyPlayed;
	}

	public void setRecentlyPlayed(String recentlyPlayed) {
		this.recentlyPlayed = recentlyPlayed;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getPictureAlt() {
		return pictureAlt;
	}

	public void setPictureAlt(String pictureAlt) {
		this.pictureAlt = pictureAlt;
	}
	
	
}
