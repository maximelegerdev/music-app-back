 package com.example.musicapp.entity;

import java.io.Serializable;
import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "artist")
public class Artist {
    
	public Artist() {
	}
	
    public Artist(Long artistId, String name, String picture, Long followers, String pictureAlt, String songIds, 
	String albumIds, String style, Long monthlyListeners, Boolean isVerified) {
		super();
		this.artistId = artistId;
		this.name = name;
		this.picture = picture;
		this.followers = followers;
		this.pictureAlt = pictureAlt;
		this.songIds = songIds;
		this.albumIds = albumIds;
		this.style = style;
		this.monthlyListeners = monthlyListeners;
		this.isVerified = isVerified;
	}

	@Id
    @Column(name="artist_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long artistId;

	@Column(name="artist_name", nullable = false)
    private String name;

    @Column(name="picture")
    private String picture;

    @Column(name="followers", nullable = false)
    private Long followers;

    @Column(name="picture_alt", nullable = false)
    private String pictureAlt;
    
    @Column(name = "song_ids")
    private String songIds;
    
    @Column(name = "album_ids")
    private String albumIds;
    
    @Column(name="style", nullable = false)
    private String style;

	@Column(name="monthly_listeners", nullable = false)
	private Long monthlyListeners;

	@Column(name="is_verified", nullable = false)
	private Boolean isVerified;

	public Long getArtistId() {
		return artistId;
	}

	public void setArtistId(Long artistId) {
		this.artistId = artistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Long getFollowers() {
		return followers;
	}

	public void setFollowers(Long followers) {
		this.followers = followers;
	}

	public String getPictureAlt() {
		return pictureAlt;
	}

	public void setPictureAlt(String pictureAlt) {
		this.pictureAlt = pictureAlt;
	}
	
	public String getSongIds() {
		return songIds;
	}

	public void setSongIds(String songIds) {
		this.songIds = songIds;
	}

	public String getAlbumIds() {
		return albumIds;
	}

	public void setAlbumIds(String albumIds) {
		this.albumIds = albumIds;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Long getMonthlyListeners() {
		return monthlyListeners;
	}

	public void setMonthlyListeners(Long monthlyListeners) {
		this.monthlyListeners = monthlyListeners;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}
}
