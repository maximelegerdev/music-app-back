package com.example.musicapp.entity;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;


@Entity 
@Table (name = "playlist")
public class Playlist {
	
	public Playlist() {
	}
	
	public Playlist(Long playlistId, String playlistName, String songsIds, int songsNumber, Time duration, User user,
			String cover, Date creationDate, Date lastListeningDate, int likes, String coverAlt, String style, String description) {
		super();
		this.playlistId = playlistId;
		this.playlistName = playlistName;
		this.songsIds = songsIds;
		this.songsNumber = songsNumber;
		this.duration = duration;
		this.user = user;
		this.cover = cover;
		this.creationDate = creationDate;
		this.lastListeningDate = lastListeningDate;
		this.likes = likes;
		this.coverAlt = coverAlt;
		this.style = style;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="playlist_id", nullable = false)
	private Long playlistId;
	
	@Column(name="playlist_name", nullable = false)
	private String playlistName;
	
	@Column(name="songs_ids")
	private String songsIds;
	
	@Column(name="songs_number")
	private int songsNumber;
	
	@Column(name="duration")
	private Time duration;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable = false)
	private User user;
	
	@Column(name="cover")
	private String cover;
	
	@Column(name="creation_date", nullable = false)
	private Date creationDate;
	
	@Column(name="lastListeningDate")
	private Date lastListeningDate;
	
	@Column(name="likes")
	private int likes;
	
	@Column(name="cover_alt")
	private String coverAlt;

	@Column(name="style", nullable = false)
    private String style;

	@Column(name="description", nullable = true)
    private String description;
	
	public Long getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(Long playlistId) {
		this.playlistId = playlistId;
	}

	public String getPlaylistName() {
		return playlistName;
	}

	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	public String getSongsIds() {
		return songsIds;
	}

	public void setSongsIds(String songsIds) {
		this.songsIds = songsIds;
	}

	public int getSongsNumber() {
		return songsNumber;
	}

	public void setSongsNumber(int songsNumber) {
		this.songsNumber = songsNumber;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastListeningDate() {
		return lastListeningDate;
	}

	public void setLastListeningDate(Date lastListeningDate) {
		this.lastListeningDate = lastListeningDate;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String getCoverAlt() {
		return coverAlt;
	}

	public void setCoverAlt(String coverAlt) {
		this.coverAlt = coverAlt;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
