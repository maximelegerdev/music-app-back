package com.example.musicapp.entity;


import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Set;


@Entity
@Table(name = "album")
public class Album {
	
	public Album() {
	}
    
    public Album(Long albumId, String albumTitle, String artistsIds, Date releaseDate, String cover, String albumType,
			String coverAlt, Time duration, int likes, String style, int songsNumber , Set<Song> songs) {
		super();
		this.albumId = albumId;
		this.albumTitle = albumTitle;
		this.artistsIds = artistsIds;
		this.releaseDate = releaseDate;
		this.cover = cover;
		this.albumType = albumType;
		this.coverAlt = coverAlt;
		this.duration = duration;
		this.likes = likes;
		this.style = style;
		this.songsNumber = songsNumber;
		this.songs = songs;
	}

	@Id
    @Column(name="album_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long albumId;

	@Column(name="album_title", nullable = false)
    private String albumTitle;

	@Column(name="artists_ids")
    private String artistsIds;
    
    @Column(name="release_date", nullable = false)
    private Date releaseDate;

    @Column(name="cover", nullable = false)
    private String cover;

    @Column(name="album_type", nullable = false)
    private String albumType;

    @Column(name="cover_alt", nullable = false)
    private String coverAlt;

    @Column(name="duration", nullable = false)
    private Time duration;

    @Column(name="likes", nullable = false)
    private int likes;
    
    @Column(name="style", nullable = false)
    private String style;
    
    @Column(name="songs_number", nullable=false)
    private int songsNumber;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy="album")
	@JsonManagedReference
    private Set<Song> songs;
    
	public Long getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Long albumId) {
		this.albumId = albumId;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getArtistsIds() {
		return artistsIds;
	}

	public void setArtistsIds(String artistsIds) {
		this.artistsIds = artistsIds;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getAlbumType() {
		return albumType;
	}

	public void setAlbumType(String albumType) {
		this.albumType = albumType;
	}

	public String getCoverAlt() {
		return coverAlt;
	}

	public void setCoverAlt(String coverAlt) {
		this.coverAlt = coverAlt;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
	
	public int getSongsNumber() {
		return songsNumber;
	}

	public void setSongsNumber(int songsNumber) {
		this.songsNumber = songsNumber;
	}
	
	public Set<Song> getSongs() {
		return songs;
	}

	public void setSongs(Set<Song> songs) {
		this.songs = songs;
	}

}

