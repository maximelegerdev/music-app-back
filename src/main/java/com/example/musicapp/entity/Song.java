package com.example.musicapp.entity;

import javax.persistence.Column;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Set;


@Entity
@Table(name = "song")

public class Song {
	
	public Song() {
	}

    public Song(Long songId, String songTitle, String artistsIds, Time duration, Album album, Date releaseDate, String song,
			String cover, Long likes, String coverAlt, String style, Long streams, boolean isExplicit) {
		super();
		this.songId = songId;
		this.songTitle = songTitle;
		this.artistsIds = artistsIds;
		this.duration = duration;
		this.album = album;
		this.releaseDate = releaseDate;
		this.song = song;
		this.cover = cover;
		this.likes = likes;
		this.coverAlt = coverAlt;
		this.style = style;
		this.streams = streams;
		this.isExplicit = isExplicit;
	}
    

	@Id
    @Column(name="song_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long songId;

	@Column(name="song_title", nullable = false)
    private String songTitle;
    
    @Column(name="artists_ids")
    private String artistsIds;

    @Column(name="duration", nullable = false)
    private Time duration;

    @ManyToOne
    @JoinColumn(name="album_id")
	@JsonBackReference
    private Album album;

    @Column(name="release_date", nullable = false)
    private Date releaseDate;
    
    @Column(name="song", nullable = false)
    private String song;

    @Column(name="cover", nullable = false)
    private String cover;
    
    @Column(name="likes", nullable = false)
    private Long likes;

    @Column(name="cover_alt", nullable = false)
    private String coverAlt;
    
    @Column(name="style", nullable = false)
    private String style;

	@Column(name="streams", nullable = false)
    private Long streams;

	@Column(name="is_explicit", nullable = false)
    private boolean isExplicit;

	public Long getSongId() {
		return songId;
	}

	public void setSongId(Long songId) {
		this.songId = songId;
	}

	public String getSongTitle() {
		return songTitle;
	}

	public void setSongTitle(String songTitle) {
		this.songTitle = songTitle;
	}

	public String getArtistsIds() {
		return artistsIds;
	}

	public void setArtistsIds(String artistsIds) {
		this.artistsIds = artistsIds;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getSong() {
		return song;
	}

	public void setSong(String song) {
		this.song = song;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public String getCoverAlt() {
		return coverAlt;
	}

	public void setCoverAlt(String coverAlt) {
		this.coverAlt = coverAlt;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Long getStreams() {
		return streams;
	}

	public void setStreams(Long streams) {
		this.streams = streams;
	}

	public boolean getIsExplicit() {
		return isExplicit;
	}

	public void setIsExplicit(boolean isExplicit) {
		this.isExplicit = isExplicit;
	}
}
